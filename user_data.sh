PUBLIC_IP=$1
PRIVATE_IP=$2
LB_IP=$3
if [[ $PUBLIC_IP =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
  echo "initialising"
else
  echo "No valid IP found"
  exit 1

fi

ssh -o "StrictHostKeyChecking=no" -i ~/.ssh/cohort8ijazssh.pem ec2-user@$PUBLIC_IP ' 
sudo sh -c "cat >/etc/hosts << _END_
127.0.0.1 localhost

# The following lines are desirable for IPv6 capable hosts
::1 ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
ff02::3 ip6-allhosts
Cohort8-Ijaz-LB $LB_IP
sudo yum update -y
sudo yum install -y httpd httpd-tools mod_ssl -y
sudo systemctl enable httpd
sudo systemctl start httpd
case $1 in
    'start')
            echo -e "<html>\nHello there<br>” >/var/www/html/index.html
		echo -e "My IP address is $(ifconfig | grep -A1 eth0 | grep inet | awk '{print $2}'| sed 's/^.*://')\n</html>" >>/var/www/html/index.html
		echo -e "Operating system aws" >>/var/www/html/index.html
		echo -e "Web server is apache and version 2.4" >>/var/www/html/index.html
            ;;
    *)
        echo "Not a valid argument"
        ;;
esac
'